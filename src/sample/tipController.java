package sample;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class tipController {

    public static final double DOLLAR_RATE = 68.82;
    public static final double EURO_RATE = 77.50;

    @FXML
    private TextField billAmountField;

    @FXML
    private TextField amountToPayField;

    @FXML
    private TextField tipToPayField;

    @FXML
    private TextField tipToPayInRublesField;

    @FXML
    private TextField toPayInRublesField;

    @FXML
    private ComboBox<String> currencyComboBox;

    @FXML
    private ComboBox<String> tipComboBox;

    @FXML
    private CheckBox doRoundCheckBox;

    @FXML
    private CheckBox printInRublesCheckBox;

    @FXML
    private Label amountInRublesToPayLabel;

    @FXML
    private Label tipToPayInRublesLabel;

    @FXML
    private Label toPayInRublesLabel;

    @FXML
    private int choiceCurrency() {
        switch (currencyComboBox.getValue()) {
            case "Доллары":
                return 2;
            case "Евро":
                return 3;
        }
        return 1;
    }

    @FXML
    private int choiceTip() {
        switch (tipComboBox.getValue()) {
            case "5%":
                return 2;
            case "10%":
                return 3;
        }
        return 1;
    }

    private boolean isNotEmpty() {
        if (billAmountField.getText().isEmpty()) {
            newShake(billAmountField);
            return false;
        }
        return true;
    }

    private Double giveBillAmount() {
        try {
            double amount = Double.parseDouble(billAmountField.getText().replace(",", "."));
            if (amount <= 0) {
                throw new Exception("Отрицательное число");
            }
            return amount;
        } catch (NumberFormatException e) {
            billAmountField.setText("Введите число");
            newShake(billAmountField);
        } catch (Exception e) {
            billAmountField.setText("Введите число больше нуля");
            newShake(billAmountField);
        }
        return null;
    }

    private double tipCalculate(double amount) {
        switch (choiceTip()) {
            case 1:
                amount = 0;
                break;
            case 2:
                amount = amount * 0.05;
                break;
            case 3:
                amount = amount * 0.1;
        }
        return amount;
    }

    private Double numberFormation(Double number) {
        return new BigDecimal(number).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    private boolean isSelected(CheckBox checkBox) {
        return checkBox.isSelected();
    }

    private double makeRound(double number) {
        return new BigDecimal(number).setScale(0, RoundingMode.HALF_UP).doubleValue();
    }

    @FXML
    void clear() {
        billAmountField.clear();
        toPayInRublesField.clear();
        tipToPayField.clear();
        tipToPayInRublesField.clear();
        amountToPayField.clear();
        doRoundCheckBox.setSelected(false);
        printInRublesCheckBox.setSelected(false);
        clearLabel(amountInRublesToPayLabel);
    }

    @FXML
    void giveResult() {
        if (isNotEmpty() && giveBillAmount() != null) {
            double amount = giveBillAmount();
            double tip = tipCalculate(amount);

            makeVisible();
            switch (choiceCurrency()) {
                case 1:
                    calculateResult(amount, tip);
                    break;
                case 2:
                    calculateResult(amount, tip);
                    calculateResultInRubles(amount, tip, DOLLAR_RATE);
                    break;
                case 3:
                    calculateResult(amount, tip);
                    calculateResultInRubles(amount, tip, EURO_RATE);
                    break;
            }
        }
    }

    private void clearLabel(Label label) {
        if (!label.getText().isEmpty()) label.setText("");
    }

    private void makeVisible() {
        if (choiceCurrency() != 1) {
            tipToPayInRublesField.setVisible(true);
            toPayInRublesField.setVisible(true);
            tipToPayInRublesLabel.setText("Чаевые в рублях:");
            toPayInRublesLabel.setText("К оплате в рублях:");
        } else {
            tipToPayInRublesField.setVisible(false);
            toPayInRublesField.setVisible(false);
            clearLabel(toPayInRublesLabel);
            clearLabel(tipToPayInRublesLabel);
            clearLabel(amountInRublesToPayLabel);
        }
    }

    private void printResult(Double number, TextField textField) {
        DecimalFormat twoDForm = new DecimalFormat("#0.00");
        textField.setText(twoDForm.format(number).replace(",", "."));
    }

    private void printResultInLabel(Double number, Label label) {
        DecimalFormat twoDForm = new DecimalFormat("#0.00");
        label.setText("Сумма чека в рублях: " + twoDForm.format(number).replace(",", "."));
    }

    private void calculateResult(double amount, double tip) {
        printResult(numberFormation(doRound(amount + tip)), amountToPayField);
        printResult(numberFormation(tip), tipToPayField);
    }

    private void calculateResultInRubles(double amount, double tip, double currency) {
        printResult(numberFormation(doRound((amount + tip) * currency)), toPayInRublesField);
        printResult(numberFormation(tip * currency), tipToPayInRublesField);
        printResultInLabel(numberFormation(amount * currency), amountInRublesToPayLabel);
    }

    private double doRound(double number) {
        if (isSelected(doRoundCheckBox)) {
            return makeRound(Math.ceil(number));
        } else {
            return number;
        }
    }

    private void newShake(TextField field) {
        Shake operandShake = new Shake(field);
        operandShake.playAnimation();
    }
}